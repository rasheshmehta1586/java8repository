package com.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExample1 {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("1","Rashesh",32),
				new Person("2","Varun",30),
				new Person("3","Arpit",33));
	
		// One approach to to first iterate list and create another list
		// Then iterate that list to print filtered elements

		// Instead of looping twice, we can use streams to create conveyor belt where person objects are kept
		// Then perform operations on those elements
		
		//people.stream().filter(p -> p.getName().startsWith("R")).forEach(System.out::println);
		
		List<String> names=people.stream().filter(p -> p.getName().startsWith("R")).map(Person::getName).map(String::toUpperCase).collect(Collectors.toList());
		System.out.println("Modifed");
		for(String name : names){
			System.out.println(name);
		}
		
		System.out.println("Original");
		for(Person person : people){
			System.out.println(person);
		}
		
		
		
	}

}
