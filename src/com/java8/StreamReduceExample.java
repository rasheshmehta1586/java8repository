package com.java8;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamReduceExample {
	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("1","Rashesh",32),
				new Person("2","Varun",30),
				new Person("3","Arpit",33),
				new Person("4","Arpit",34));
		
		System.out.println("Sum of all ages");
		System.out.println(people.stream().map(Person::getAge).reduce(0,(carry,age) -> carry+age));// 0 is initial value of result
		
		System.out.println("Sum of all ages through specialized function");
		System.out.println(people.stream().mapToInt(Person::getAge).sum());
		
		System.out.println("Concetenate all ages");
		System.out.println(people.stream().map(Person::getName).reduce("", (carry,name) -> carry+name));
		
		System.out.println("List of all names");
		List<String> names = people.stream().map(Person::getName).collect(Collectors.toList());
		for(String name : names){
			System.out.println(name);
		}
		
		System.out.println("List of all distinct names");
		Set<String> disNames = people.stream().map(Person::getName).collect(Collectors.toSet());
		for(String name : disNames){
			System.out.println(name);
		}
		
		System.out.println("Making map");
		Map<String,Person> map = people.stream().collect(Collectors.toMap(Person::getId,p -> p));
		
		map.forEach((k,v) -> System.out.println("Key "+k+"value = "+v));
		
		System.out.println("Making map using group by");
		
		Map<String,List<Person>> map1 = people.stream().collect(Collectors.groupingBy(Person::getName));
		
		
	}
	
}
