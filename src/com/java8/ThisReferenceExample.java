package com.java8;

import java.util.function.Consumer;

public class ThisReferenceExample {
	
	public void doProcess(int n, Consumer<Integer> consumer){
		consumer.accept(n);
	}
	
	public void process(){
		doProcess(5, n -> {
			System.out.println("Value of n is "+n);
			System.out.println(this);
		});
	}
	
	@Override
	public String toString(){
		return "This is ThisReferenceExample class instance and not an instance of Consumer interface";
	}
	
	public static void main(String[] args) {
		ThisReferenceExample obj = new ThisReferenceExample();
		//obj.process();
		obj.processWithInnerClass();
	}

	private void processWithInnerClass() {
		doProcess(5, new Consumer<Integer>(){

			@Override
			public void accept(Integer n) {
				System.out.println("Value of n is "+n);
				System.out.println(this);
				
			}
			
			@Override
			public String toString(){
				return "This is an instance of Consumer interface";
			}
		});
		
	}
}
