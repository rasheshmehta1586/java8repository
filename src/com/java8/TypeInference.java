package com.java8;

public class TypeInference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringLength stringLengthLabda = str -> str.length();
		printLamda(stringLengthLabda);
	}
	
	public static void printLamda(StringLength lambda){
		System.out.println(lambda.getLength("HelloWorld"));
	}

}

interface StringLength{
	int getLength(String str);
}
