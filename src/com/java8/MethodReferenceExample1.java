package com.java8;

public class MethodReferenceExample1 {


	public static void main(String[] args) {
		//printMessage();
		
		Thread t1 =  new Thread(MethodReferenceExample1 :: printMessage); // () -> printMessage()
		t1.start();
	}

	private static void printMessage() {
		System.out.println("Hello World");
		
	}

}
