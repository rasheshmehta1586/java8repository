package com.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MethodReferenceExample2 {
	
	static List<Person> people = Arrays.asList(
			new Person("1","Rashesh",32),
			new Person("2","Varun",30),
			new Person("3","Arpit",33));
	
	public static void main(String[] args) {
		MethodReferenceExample2 obj = new MethodReferenceExample2();
		obj.process(people,p -> p.getName().startsWith("R"),System.out::println); //(p) -> method(p)
	}

	private void process(List<Person> people,Predicate<Person> predicate,Consumer<Person> consumer) {
		people.forEach(person -> {
			if(predicate.test(person))
				consumer.accept(person);
		});
		
	}
	
	
}


