package com.java8;

public class LambdaExpression {

	public static void main(String[] args) {
	
	LambdaExpression lambdaExpression = new LambdaExpression();
	Greeting lambda = 	() -> System.out.println("Hello World!!");
	
	lambdaExpression.greet(lambda);
	
	}

	public void greet(Greeting greeting){
		greeting.greet();
	}
}


interface Greeting{
	void greet();
}



