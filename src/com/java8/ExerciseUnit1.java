package com.java8;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class ExerciseUnit1 {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("1","Rashesh",32),
				new Person("2","Varun",30),
				new Person("3","Arpit",33));

		Collections.sort(people, (Person p1, Person p2) -> p1.getName().compareTo(p2.getName()));
		
		//printList(people);
		printListOnCondition(people, p -> true);
		printListOnCondition(people, p -> p.getName().startsWith("V"));
	}

	private static void printListOnCondition(List<Person> people, Predicate<Person> predicate) {
		people.forEach(element -> {
			if(predicate.test(element)){
				System.out.println(element);
			}
		});
		
	}

	private static void printList(List<Person> people) {
		people.forEach(element -> System.out.println(element));
	}

}

//interface Condition{
//	boolean test(Person p);
//}