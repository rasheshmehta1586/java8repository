package com.java8;

import java.util.function.Consumer;

public class Closure {

	public static void main(String[] args) {
		int n = 5;
		int a = 10;
		
		process(n, n1 -> {
			n1 = a;
			System.out.println(n1+a);
		});

		n = 11;
	}

	private static void process(int n, Consumer<Integer> consume) {
		consume.accept(n);
		
	}

}
