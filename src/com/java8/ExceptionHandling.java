package com.java8;

import java.util.function.BiConsumer;

public class ExceptionHandling {

	public static void main(String[] args) {
		int[] numbers = {1,2,3,4};
		int key = 0;
		
		calculate(numbers,key, wrapperLamda((n,k) -> System.out.println(n/k)));
	}
	
	public static void calculate(int[] numbers, int key, BiConsumer<Integer, Integer> biConsumer){
		for(int n : numbers){
			biConsumer.accept(n, key);
		}
	
	}

	public static BiConsumer<Integer, Integer> wrapperLamda(BiConsumer<Integer, Integer> biConsumer){
		return (n,k) -> {
			try{
				biConsumer.accept(n, k);
			}
			catch(Exception ex){
				System.out.println(ex);
			}
		};
	}
}
